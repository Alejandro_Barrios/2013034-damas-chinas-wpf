﻿Class MainWindow

#Region "Variables_Globales"

    ' Aqui se declaran las variables globales como:
    ' posiciones, la matriz y el buscador de la pieza a mover

    Dim nuevaVentana As New Window1
    Dim posicionX As Integer
    Dim posicionY As Integer
    Dim posicionYMover As Integer
    Dim posicionXMover As Integer
    Dim selection(59) As UserControl
    Dim selection3(44) As UserControl
    Dim contadorPiece As Integer = 0
    Dim contadorPiece3 As Integer = 0
    Dim buscador As Integer
    Dim piezaMover As Object = Nothing
    Dim piezaBrincar As Object = Nothing
    Dim selectionElip(181) As Object
    Dim contadorElip As Integer = 0
    Dim parteTablero As Object = Nothing
    Dim juego3 As Boolean = False
    Dim juego6 As Boolean = False
    Dim piezaBuscar As Object = Nothing
    Dim contarTurno As Integer = 1
    Dim contarTurno3 As Integer = 1

#End Region

#Region "Metodo_LoadBoard"
    ' Aqui se crea la tabla de juego

    Private Sub LoadBoard()

        ' Variables para poder crear la tabla
        ' Esta hecha por 2 triangulos, las primeras 3 variables son del primer triangulo
        ' Las segundas 3 variables son del triangulo invertido

        Dim left As Integer = 170
        Dim top As Integer = 12
        Dim contador As Integer = 0
        '------------------------------
        Dim leftAscend As Integer = 170
        Dim topAscend As Integer = 380
        Dim contador2 As Integer = 0

        For y As Integer = 0 To 12
            For x As Integer = 0 To contador
                Dim elipse As New Ellipse
                elipse.Width = 20
                elipse.Height = 20
                elipse.Fill = Brushes.Black
                elipse.Stroke = Brushes.Black
                Canvas.SetLeft(elipse, left)
                Canvas.SetTop(elipse, top)
                AddHandler elipse.MouseLeftButtonDown, AddressOf TableClick
                tablero.Children.Add(elipse)
                selectionElip(contadorElip) = elipse
                contadorElip += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top += 23
            contador += 1
        Next

        For yNew As Integer = 0 To 12
            For xNew As Integer = 0 To contador2
                Dim elipse2 As New Ellipse
                elipse2.Width = 20
                elipse2.Height = 20
                elipse2.Fill = Brushes.Black
                elipse2.Stroke = Brushes.Black
                Canvas.SetLeft(elipse2, leftAscend)
                Canvas.SetTop(elipse2, topAscend)
                AddHandler elipse2.MouseLeftButtonDown, AddressOf TableClick
                tablero.Children.Add(elipse2)
                selectionElip(contadorElip) = elipse2
                contadorElip += 1
                leftAscend += 26
            Next
            leftAscend = leftAscend - ((26 * (contador2 + 1) + 13))
            topAscend -= 23
            contador2 += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadRed"
    ' Aqui se crean las fichas rojas

    Private Sub LoadRed()

        ' Aqui se declaran las variables para crear las fichas

        Dim contador As Integer = 0
        Dim left As Integer = 170
        Dim top As Integer = 12

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim redN As New Rojo()
                Canvas.SetLeft(redN, left)
                Canvas.SetTop(redN, top)
                AddHandler redN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(redN)
                selection(contadorPiece) = redN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top += 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadGreen"

    ' Aqui se crean las fichas verdes del juego de 6

    Private Sub LoadGreen()

        ' Aqui se declaran las variables que se utilizaran para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 170
        Dim top As Integer = 380

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim greenN As New Verde
                Canvas.SetLeft(greenN, left)
                Canvas.SetTop(greenN, top)
                AddHandler greenN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(greenN)
                selection(contadorPiece) = greenN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadBlue"

    ' Aqui se crean las fichas azules del juego de 6

    Private Sub LoadBlue()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 287
        Dim top As Integer = 173

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim BlueN As New Azul
                Canvas.SetLeft(BlueN, left)
                Canvas.SetTop(BlueN, top)
                AddHandler BlueN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(BlueN)
                selection(contadorPiece) = BlueN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadOrange"

    ' Aqui se crean las fichas naranjas del juego de 6

    Private Sub LoadOrange()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 53
        Dim top As Integer = 173

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim OrangeN As New Naranja
                Canvas.SetLeft(OrangeN, left)
                Canvas.SetTop(OrangeN, top)
                AddHandler OrangeN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(OrangeN)
                selection(contadorPiece) = OrangeN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadYellow"

    ' Aqui se crean las fichas amarillas

    Private Sub LoadYellow()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 53
        Dim top As Integer = 219

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim YellowN As New Amarillo
                Canvas.SetLeft(YellowN, left)
                Canvas.SetTop(YellowN, top)
                AddHandler YellowN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(YellowN)
                selection(contadorPiece) = YellowN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top += 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadGray"

    ' Aqui se crean las fichas Grises

    Private Sub LoadGray()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 287
        Dim top As Integer = 219

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim GrayN As New Gris
                Canvas.SetLeft(GrayN, left)
                Canvas.SetTop(GrayN, top)
                AddHandler GrayN.MouseLeftButtonDown, AddressOf ClickUControl
                tablero.Children.Add(GrayN)
                selection(contadorPiece) = GrayN
                contadorPiece += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top += 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadGreen3"

    ' Aqui se crean las fichas verdes del juego de 3

    Private Sub LoadGreen3()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 170
        Dim top As Integer = 380

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim greenN3 As New Verde
                Canvas.SetLeft(greenN3, left)
                Canvas.SetTop(greenN3, top)
                AddHandler greenN3.MouseLeftButtonDown, AddressOf ClickUControl3
                tablero.Children.Add(greenN3)
                selection3(contadorPiece3) = greenN3
                contadorPiece3 += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadOrange3"

    ' Aqui se crean las fichas del juego de 3

    Private Sub LoadOrange3()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 66
        Dim top As Integer = 196

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim OrangeN3 As New Naranja
                Canvas.SetLeft(OrangeN3, left)
                Canvas.SetTop(OrangeN3, top)
                AddHandler OrangeN3.MouseLeftButtonDown, AddressOf ClickUControl3
                tablero.Children.Add(OrangeN3)
                selection3(contadorPiece3) = OrangeN3
                contadorPiece3 += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_LoadBlue3"

    ' Aqui se crean las fichas azules del juego de 3

    Private Sub LoadBlue3()

        ' Aqui se declaran las variables para crearlas

        Dim contador As Integer = 0
        Dim left As Integer = 274
        Dim top As Integer = 196

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim BlueN3 As New Azul
                Canvas.SetLeft(BlueN3, left)
                Canvas.SetTop(BlueN3, top)
                AddHandler BlueN3.MouseLeftButtonDown, AddressOf ClickUControl3
                tablero.Children.Add(BlueN3)
                selection3(contadorPiece3) = BlueN3
                contadorPiece3 += 1
                left += 26
            Next
            left = left - ((26 * (contador + 1) + 13))
            top -= 23
            contador += 1
        Next

    End Sub

#End Region

#Region "Metodo_Tablero_Initialized"

    ' En este metodo se inicializa el tablero

    Private Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles tablero.Initialized

        LoadBoard()

        For a As Integer = 0 To UBound(selectionElip)
            selectionElip(a).IsEnabled = False

        Next

    End Sub

#End Region

#Region "Metodo_Btn6Players_Click"

    ' Al apachar este boton se habilita el juego de 6

    Public Sub Btn6Players_Click() Handles btn6Players.Click

        MsgBox("Para poder llevar control de los turnos del juego Ingrese los nombres de los jugadores y presione aceptar", MsgBoxStyle.Exclamation)

        nuevaVentana.Show()

        nuevaVentana.name1.IsEnabled = True
        nuevaVentana.name2.IsEnabled = True
        nuevaVentana.name3.IsEnabled = True
        nuevaVentana.name4.IsEnabled = True
        nuevaVentana.name5.IsEnabled = True
        nuevaVentana.name6.IsEnabled = True

        LoadRed()
        LoadGreen()
        LoadBlue()
        LoadOrange()
        LoadYellow()
        LoadGray()
        btn3Players.IsEnabled = False
        btn6Players.IsEnabled = False
        btnReset.IsEnabled = True
        juego6 = True

        For a As Integer = 0 To UBound(selectionElip)
            selectionElip(a).IsEnabled = True

        Next

    End Sub

#End Region

#Region "Metodo_Btn3Players_Click"

    ' Al apachar este boton se habilita el juego de 3

    Private Sub Btn3Players_Click(sender As Object, e As RoutedEventArgs) Handles btn3Players.Click

        MsgBox("Para poder llevar control de los turnos del juego Ingrese los nombres de los jugadores y presione aceptar", MsgBoxStyle.Exclamation)

        nuevaVentana.Show()

        nuevaVentana.name1.IsEnabled = True
        nuevaVentana.name2.IsEnabled = True
        nuevaVentana.name3.IsEnabled = True

        LoadBlue3()
        LoadOrange3()
        LoadGreen3()
        btn3Players.IsEnabled = False
        btn6Players.IsEnabled = False
        btnReset.IsEnabled = True
        juego3 = True

        For a As Integer = 0 To UBound(selectionElip)
            selectionElip(a).IsEnabled = True

        Next

    End Sub

#End Region

#Region "Marcar_Posibles_Movimientos_Juego6"

    ' Este metodo obtiene la posicion de las fichas del juedo de 6 al hacer click sobre ellas

    Private Sub ClickUControl(sender As Object, e As RoutedEventArgs)

        For a As Integer = 0 To UBound(selectionElip)

            selectionElip(a).fill = Brushes.Black

        Next

        For buscar As Integer = 0 To UBound(selection)

            If Canvas.GetLeft(sender) = Canvas.GetLeft(selection(buscar)) And
                Canvas.GetTop(sender) = Canvas.GetTop(selection(buscar)) Then

                piezaMover = selection(buscar)

            End If

        Next

        For buscarElipse As Integer = 0 To UBound(selectionElip)

            If Canvas.GetLeft(piezaMover) = Canvas.GetLeft(selectionElip(buscarElipse)) And
                Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscarElipse)) Then

                piezaBrincar = selectionElip(buscarElipse)

            End If

        Next



        For buscar As Integer = 0 To UBound(selectionElip)

            For buscarTope As Integer = 0 To UBound(selection)

                ' Mover Arriba derecha

                If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selection(buscarTope)) And
                    Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = parteTablero

                            If Canvas.GetLeft(piezaBrincar) + 13 = Canvas.GetLeft(selection(buscarTope)) And
                                Canvas.GetTop(piezaBrincar) + -23 = Canvas.GetTop(selection(buscarTope)) Then

                                If Canvas.GetLeft(piezaBrincar) + 26 = Canvas.GetLeft(selection(buscarTope)) And
                                    Canvas.GetTop(piezaBrincar) + -46 = Canvas.GetTop(selection(buscarTope)) Then

                                Else

                                    If Canvas.GetLeft(piezaBrincar) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                                        Canvas.GetTop(piezaBrincar) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                                        parteTablero = selectionElip(buscar)
                                        parteTablero.Fill = Brushes.Pink
                                        piezaBrincar = selectionElip(buscar)

                                    End If

                                End If

                            End If

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Arriba izquierda

                If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selection(buscarTope)) And
                         Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink


                    End If

                End If

                ' Mover izquierda

                If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection(buscarTope)) And
                         Canvas.GetTop(piezaMover) = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -52 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -52 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) - 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Derecha

                If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection(buscarTope)) And
                         Canvas.GetTop(piezaMover) = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 52 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 52 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Abajo Izquierda

                If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selection(buscarTope)) And
                         Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover abajo derecha

                If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selection(buscarTope)) And
                         Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selection(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection(buscarTope)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selection(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

            Next

        Next

    End Sub

#End Region

#Region "Marcar_Posibles_Movimientos_Juego3"

    ' Este metodo obtiene la posicion de las fichas del juego de 3 al hacer click sobre ellas

    Private Sub ClickUControl3(sender As Object, e As RoutedEventArgs)

        For a As Integer = 0 To UBound(selectionElip)

            selectionElip(a).fill = Brushes.Black

        Next

        For buscar As Integer = 0 To UBound(selection3)

            If Canvas.GetLeft(sender) = Canvas.GetLeft(selection3(buscar)) And
                Canvas.GetTop(sender) = Canvas.GetTop(selection3(buscar)) Then

                piezaMover = selection3(buscar)

            End If

        Next

        For buscarElipse As Integer = 0 To UBound(selectionElip)

            If Canvas.GetLeft(piezaMover) = Canvas.GetLeft(selectionElip(buscarElipse)) And
                Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscarElipse)) Then

                piezaBrincar = selectionElip(buscarElipse)

            End If

        Next

        For buscar As Integer = 0 To UBound(selectionElip)

            For buscarTope As Integer = 0 To UBound(selection3)

                ' Mover Arriba derecha

                If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selection3(buscarTope)) And
                    Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = parteTablero

                            If Canvas.GetLeft(piezaBrincar) + 13 = Canvas.GetLeft(selection3(buscarTope)) And
                                Canvas.GetTop(piezaBrincar) + -23 = Canvas.GetTop(selection3(buscarTope)) Then

                                If Canvas.GetLeft(piezaBrincar) + 26 = Canvas.GetLeft(selection3(buscarTope)) And
                                    Canvas.GetTop(piezaBrincar) + -46 = Canvas.GetTop(selection3(buscarTope)) Then

                                Else

                                    If Canvas.GetLeft(piezaBrincar) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                                        Canvas.GetTop(piezaBrincar) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                                        parteTablero = selectionElip(buscar)
                                        parteTablero.Fill = Brushes.Pink
                                        piezaBrincar = selectionElip(buscar)

                                    End If

                                End If

                            End If

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Arriba izquierda

                If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selection3(buscarTope)) And
                         Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + -23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink


                    End If

                End If

                ' Mover izquierda

                If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection3(buscarTope)) And
                         Canvas.GetTop(piezaMover) = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -52 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -52 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) - 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Derecha

                If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection3(buscarTope)) And
                         Canvas.GetTop(piezaMover) = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 52 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 52 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover Abajo Izquierda

                If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selection3(buscarTope)) And
                         Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + -26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + -13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

                ' Mover abajo derecha

                If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selection3(buscarTope)) And
                         Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selection3(buscarTope)) Then

                    If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selection3(buscarTope)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selection3(buscarTope)) Then

                    Else

                        If Canvas.GetLeft(piezaMover) + 26 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 46 = Canvas.GetTop(selectionElip(buscar)) Then

                            parteTablero = selectionElip(buscar)
                            parteTablero.Fill = Brushes.Pink
                            piezaBrincar = selectionElip(buscar)

                        End If

                    End If

                Else

                    If Canvas.GetLeft(piezaMover) + 13 = Canvas.GetLeft(selectionElip(buscar)) And
                        Canvas.GetTop(piezaMover) + 23 = Canvas.GetTop(selectionElip(buscar)) Then

                        parteTablero = selectionElip(buscar)
                        parteTablero.Fill = Brushes.Pink

                    End If

                End If

            Next

        Next

    End Sub

#End Region

#Region "Mover_Ficha"

    ' Este metodo mueve la ficha por el tablero y obtiene la poscicion del tablero

    Private Sub TableClick(sender As Object, e As RoutedEventArgs)

        For buscar As Integer = 0 To UBound(selectionElip)
            If Canvas.GetLeft(sender) = Canvas.GetLeft(selectionElip(buscar)) And
                Canvas.GetTop(sender) = Canvas.GetTop(selectionElip(buscar)) Then
                parteTablero = selectionElip(buscar)
            End If
        Next

        If IsNothing(piezaMover) Then

            MsgBox("Seleccione una pieza", MsgBoxStyle.Critical)

        ElseIf Not IsNothing(piezaMover) Then

            If parteTablero.Fill.Equals(Brushes.Pink) Then

                Canvas.SetLeft(piezaMover, Canvas.GetLeft(parteTablero))
                Canvas.SetTop(piezaMover, Canvas.GetTop(parteTablero))
                piezaMover = Nothing

                If contarTurno = 6 Then
                    contarTurno = 0
                End If
                contarTurno = contarTurno + 1
                OrganizarTurnos()

            End If


        Else
            MsgBox("Movimiento no permitido", MsgBoxStyle.Critical)
            piezaMover = Nothing

        End If

            For a As Integer = 0 To UBound(selectionElip)
                selectionElip(a).fill = Brushes.Black

            Next


    End Sub

#End Region

#Region "Turnos"

    Public Sub OrganizarTurnos()

            If contarTurno = 1 Then

                lbEditTurno.Content = "Rojo " & nuevaVentana.guardName1

            ElseIf contarTurno = 2 Then

                lbEditTurno.Content = "Naranja " & nuevaVentana.guardName2


            ElseIf contarTurno = 3 Then

                lbEditTurno.Content = "Amarillo " & nuevaVentana.guardName3


            ElseIf contarTurno = 4 Then

                lbEditTurno.Content = "Verde " & nuevaVentana.guardName4

            ElseIf contarTurno = 5 Then

                lbEditTurno.Content = "Gris " & nuevaVentana.guardName5

            ElseIf contarTurno = 6 Then

                lbEditTurno.Content = "Azul " & nuevaVentana.guardName6

            End If

    End Sub

#End Region

#Region "Reiniciar_Juego"

    ' Este metodo recarga todo

    Private Sub BtnReset_Click(sender As Object, e As RoutedEventArgs) Handles btnReset.Click

        If juego6 = True Then

            For remover As Integer = 0 To selection.Length() - 1
                tablero.Children.Remove(selection(remover))
            Next

        ElseIf juego3 = True Then

            For remover As Integer = 0 To selection3.Length() - 1
                tablero.Children.Remove(selection3(remover))
            Next

        End If

        btn3Players.IsEnabled = True
        btn6Players.IsEnabled = True
        btnReset.IsEnabled = False
        posicionX = 0
        posicionY = 0
        posicionYMover = 0
        posicionXMover = 0
        contadorPiece = 0
        contadorPiece3 = 0
        buscador = 0
        piezaMover = Nothing
        contadorElip = 0
        parteTablero = Nothing
        juego3 = False
        juego6 = False

    End Sub

#End Region

End Class